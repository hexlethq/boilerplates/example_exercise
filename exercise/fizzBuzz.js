// Код за пределами маркеров BEGIN и END попадёт на прод, если данный файл не прописан в actions.yml.
// Библиотека lodash подключается в базовом образе.
import _ from 'lodash';

// BEGIN
// Код внутри этих маркеров практики будет вырезан при старте.
const fizzBuzz = (begin, end) => {
  for (let i = begin; i <= end; i += 1) {
    const maybeFizz = i % 3 === 0 ? 'Fizz' : '';
    const maybeBuzz = i % 5 === 0 ? 'Buzz' : '';
    const output = `${maybeFizz}${maybeBuzz}`;
    console.log(output === '' ? i : output);
  }
};

export default fizzBuzz;
// END
