# Обязательно используем один из базовых образов Хекслета:
# javascript, php, python, java, go, ruby, ...
FROM hexlet/hexlet-javascript

# При необходимости доустанавливаем нужные пакеты в систему.

# Используем скрипт apt-install
RUN apt-install tree

# Аналогичный скрипт для npm – npm-install
RUN npm-install sinon

# Пробрасываем переменную окружения в практику
# с помощью префикса X_FORWARDED.
# Переменная окружения будет доступна внутри практики без префикса
ENV X_FORWARDED_EXAMPLE=1
